'use strict';

let getRandomString = function(mini, maxi) {
  let tmp = Math.ceil(Math.random()*(maxi-mini))+mini;
  let helper = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let string = '';
  for (let i = 0; i < tmp; i++) {
    let rand = Math.floor(Math.random()*59);
    string+=helper[rand];
  }
  return string;
}

let CronJob = require('cron').CronJob;
new CronJob('0 0 12 * * *', function() {
  process.env.SECRET = getRandomString(50,60);
  process.env.SECRET2 = getRandomString(50,60);
  console.log(process.env.SECRET);
  console.log(process.env.SECRET2);
}, null, true, 'America/Los_Angeles');

module.exports = {
  tokenAuth: {
    rerollSecret2: function() {
      process.env.SECRET2 = getRandomString(50,60);
      console.log(process.env.SECRET2);         
    }
  }
};
