'use strict';
const ensureAuthenticated = require('../config/ensureAuthenticated')
const express = require('express')
const jwt = require('jsonwebtoken')
const keys = require('../config/keys')
const router = express.Router()
const passport = require('passport');
const User = require('../models/user');
const sanitize = require('../config/sanitize');
const nodemailer = require('nodemailer');

router.get('/', function(req, res, next) {
  res.render('views/index.html')
})

// Register User
router.post('/register', function(req,res,next) {
  let email = req.body.email;
  let username = req.body.username;
  let password = req.body.password;
  let password2 = req.body.password2;
  //Validation:
  req.checkBody('email', 'Email is required').notEmpty();
  req.checkBody('email', 'Email is invalid').isEmail();
  req.checkBody('username', 'Username is required').notEmpty();
  req.checkBody('password', 'Password is required').notEmpty();
  req.checkBody('password2', 'Passwords do not match').equals(password);
  let errors = req.validationErrors();
  if(errors) return res.json(errors);
  if(!sanitize.verifyString(email)) return res.sendStatus(401);
  if(!sanitize.verifyString(username)) return res.sendStatus(401);
  if(!sanitize.verifyString(password)) return res.sendStatus(401);

  let newUser = new User({
    email: email,
    username: username,
    password: password
  });
  User.createUser(newUser, function(err, user) {
    if(err) {
      if(err.code == "11000") return res.sendStatus(418);
      else throw err;
    } else res.sendStatus(201);
  });
});

// Login
router.post('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) return next(err);
    if (!user) return res.sendStatus(401);
    //user has authenticated correctly
    let random = getRandomString(10, 100);
    let token = jwt.sign({ username: user.username, id: user._id, sub: random}, process.env.SECRET, {expiresIn: '2h'});
    res.json({ token : token, user: {username: user.username, cezar: user.cezar, _id: user._id, settings: user.settings}});
  })(req, res, next);
});

let getRandomString = function(mini, maxi) {
  let tmp = Math.ceil(Math.random()*(maxi-mini))+mini;
  let helper = "1234567890abcdefghijklmnopqrstuvwxyz";
  let string = '';
  for (let i = 0; i < tmp; i++) {
    let rand = Math.floor(Math.random()*30);
    string+=helper[rand];
  }
  return string;
}

// Change password
router.post('/changePassword', ensureAuthenticated, function(req, res, next) {
  req.body.username = req.auth.username;
  if(!sanitize.verifyString(req.body.newPassword)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.password)) return res.sendStatus(401);
  passport.authenticate('local', function(err, user, info) {
    if (err) return next(err);
    if (!user) return res.sendStatus(401);
    User.changePasswd(user._id, req.body.newPassword, function(err, suc) {
      if(err) return res.sendStatus(401);
      return res.sendStatus(201);
    })
  })(req, res, next);
})

// Change user's color settings
router.post('/updColorSettings', ensureAuthenticated, function(req, res) {
  if(!sanitize.verifyString(req.body.color1)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.color2)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.color3)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.color4)) return res.sendStatus(401);
  User.findOneAndUpdate({_id: req.auth.id},
  {$set:{"settings.color1": req.body.color1, "settings.color2": req.body.color2, "settings.color3": req.body.color3, "settings.color4": req.body.color2}}, {returnNewDocument: false}, function(err, user) {
    if(err) return res.sendStatus(404);
    console.log(err);
    console.log(user);
    res.sendStatus(201);
  })
})

// Reset password
router.post('/resetPassword', function(req, res) {
  console.log(req.body);
  if(!sanitize.verifyString(req.body.email)) return res.sendStatus(401);
  if(!sanitize.verifyNumber(req.body.lang)) return res.sendStatus(401);
  User.findOne({'email': req.body.email}, function(err,user) {
    if(err) return res.sendStatus(401);
    if(!user) return res.sendStatus(418);
    let random = getRandomString(10, 100);
    let token = jwt.sign({ username: user.username, id: user._id, sub: random}, process.env.SECRET2, {expiresIn: '5m'});
    // here we send the mail
    let transporter = nodemailer.createTransport({
      host: process.env.MAIL_HOST,
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASSWORD
      },
      tls: {
        rejectUnauthorized: false
      }
    });
    // setup email data with unicode symbols
    let text, subject;
    if(req.body.lang==1) {
      text = `<b>Hello ${user.username}</b><p>Someone, probably You, requestet a password reset for Your Youlang account. Click the link below to enter a new password. Please notice that the link is only active for 5 minutes.</p><a href="https://youlang.pl/restore?seq=${token}">Password reset</a><p>In case it was not You who requested a password reset, please contact our support at <a href="mailto:mikolaj@cankudis.net">this email adress</a></p>`;
      subject = "Password reset";
    }
    else if (req.body.lang==2) {
      text = `<b>Hallo ${user.username}</b><p>Jemand, höhstwahrscheinlich Du, hat ein Kennwortreset für dein Youlang Konto beantragt. Klicke den Link um ein neues Kennwort eingeben zu können. Bitte beachte, dass der Link nur 5 Minuten aktiv ist.</p><a href="https://youlang.pl/de/${token}">Kennwortreset</a><p>Sollte es eine andere Person sein die das Reset ohne dein Wissen beantragt hat, informiere darüber die Administration <a href="mailto:mikolaj@cankudis.net">unter dieser Emailadresse</a></p>`;
      subject = "Kennwortreset";
    }
    else {
      text = `<b>Cześć ${user.username}</b><p>Ktoś, najprawdopodobniej Ty, zażądał resetu hasła dla Twojego konta w serwisie Youlang. Kliknij w link aby ustawić nowe hasło. Zwróć uwagę, że jest on aktywny jedynie przez 5 minut.</p><a href="https://youlang.pl/${token}">Resetuj hasło</a><p>Jeśli jednak to nie byłeś/aś Ty, skontaktuj się z administracją pod <a href="mailto:mikolaj@cankudis.net">tym adresem email</a>.</p>`;
      subject = "Reset hasła";
    }
    let mailOptions = {
        from: '"noreply-YouLang" <noreply@youlang.potikreff.pl>',
        to: req.body.email,
        subject: subject,
        html: text
    };
    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) return res.sendStatus(401);
        res.sendStatus(201);
    });
  })
})

router.get('/restore', function(req, res) {
  let token = req.query.seq;
  jwt.verify(token, process.env.SECRET2, function(err, token) {
    if(err) return res.sendStatus(401);
    else res.render('views/changepass.html');
  });
})

router.get('/de/:seq', function(req, res) {
  let token = req.params.seq;
  jwt.verify(token, process.env.SECRET2, function(err, token) {
    if(err) return res.sendStatus(401);
    else res.render('views/changepass-de.html');
  });
})
router.get('/en/:seq', function(req, res) {
  let token = req.params.seq;
  jwt.verify(token, process.env.SECRET2, function(err, token) {
    if(err) return res.sendStatus(401);
    else res.render('views/changepass-en.html');
  });
})

router.post('/resetChangePass', function(req,res) {
  if(!sanitize.verifyString(req.body.password)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.password2)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.token)) return res.sendStatus(401);
  console.log('test2');
  jwt.verify(req.body.token, process.env.SECRET2, function(err, user) {
    console.log(err);
    console.log(user);
    if(err) return res.sendStatus(401);
    if(!user) return res.sendStatus(401);
    User.changePasswd(user.id, req.body.password, function(err, success) {
      if(err) return res.sendStatus(401);
      console.log(success);
      console.log(process.env.SECRET2);
      keys.tokenAuth.rerollSecret2();
      console.log(process.env.SECRET2);
      return res.sendStatus(201);
    })
  })
})

// Get user by id
router.get('/users/:id', ensureAuthenticated, function(req, res) {
  let id = req.params.id;
  if(!sanitize.verifyWord(id)) return res.sendStatus(401);
  User.findOne({_id: id}).select('username').select('_id').select('cezar').exec(function(err, user) {
    if(err) return res.sendStatus(401);
    if(!user) return res.sendStatus(404);
    res.json(user);
  })
})

// Get user by name
router.get('/user/:name', ensureAuthenticated, function(req, res) {
  let username = req.params.name;
  if(!sanitize.verifyString(username)) return res.sendStatus(401);
  User.findOne({username: username}).select('username').select('_id').select('cezar').exec(function(err, user) {
    if(err) return res.sendStatus(401);
    if(!user) return res.sendStatus(404);
    res.json(user);
  })
})

module.exports = router
