"use strict";
const express = require('express');
const router = express.Router();
const ensureAuthenticated = require('../config/ensureAuthenticated');
const Word = require('../models/word');
const Topic = require('../models/topic');
const Branch = require('../models/branch');
const Group = require('../models/group');
const User = require('../models/user');
const Notification = require('../models/notification');
const Homework = require('../models/homework');
const sanitize = require('../config/sanitize');

router.get('/', ensureAuthenticated, function(req, res, next) {
  res.json('This is going to be the api')
  // this route will probably provide the whole set (words+topics+branches) for a specified group
})

router.get('/words', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.headers['x-group'];
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      Word.find({group: grp._id}, function(err, words) {
        if(err) return res.sendStatus(404);
        res.json(words);
      })
    } else return res.sendStatus(401);
  })
})

router.get('/topics', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.headers['x-group'];
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      Topic.find({group: grp._id}, function(err, topics) {
        if(err) return res.sendStatus(404);
        res.json(topics)
      })
    } else return res.sendStatus(401);
  })
})

router.get('/branches', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.headers['x-group'];
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      Branch.find({group: grp._id}, function(err, branches) {
        if(err) return res.sendStatus(404);
        res.json(branches)
      })
    } else return res.sendStatus(401);
  })
})

router.post('/createBranch', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.name)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.htmlId)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.bIndexNr)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.note)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      let newBranch = new Branch({
        name: req.body.name,
        htmlId: req.body.htmlId,
        bIndexNr: req.body.bIndexNr,
        note: req.body.note
      });
      newBranch.group = grp._id;
      newBranch.createdBy = req.auth.username;
      newBranch.save(function(err, branch) {
        if(err) return res.sendStatus(404);
        notify(grp.users, 'b', grp.name, req.auth.username, req.auth.id);
        res.json(branch)
      })
    } else return res.sendStatus(401);
  })
})

router.post('/createTopic', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.name)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.htmlId)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.indexNr)) return res.sendStatus(401);
  if(!sanitize.verifyWord(req.body.parentBranch)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.note)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      let newTopic = new Topic({
        name: req.body.name,
        htmlId: req.body.htmlId,
        indexNr: req.body.indexNr,
        parentBranch: req.body.parentBranch,
        note: req.body.note
      });
      newTopic.group = grp._id;
      newTopic.createdBy = req.auth.username;
      newTopic.save(function(err, topic) {
        if(err) res.send(err)
        notify(grp.users, 't', grp.name, req.auth.username, req.auth.id);
        res.json(topic)
      })
    } else return res.sendStatus(401);
  })
})

router.post('/createWord', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyWord(req.body.topic)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.word)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.text)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.explaination)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      let newWord = new Word({
        word: req.body.word,
        explaination: req.body.explaination,
        topic: req.body.topic,
        text: req.body.text
      });
      newWord.group = grp._id;
      newWord.createdBy = req.auth.username;
      newWord.save(function(err, word) {
        if(err) return res.send(err)
        notify(grp.users, 'w', grp.name, req.auth.username, req.auth.id);
        res.json(word)
      })
    } else return res.sendStatus(401);
  })
})

router.post('/updateBranch', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyWord(req.body.id)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.name)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.note)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {
      Branch.findOneAndUpdate({_id: req.body.id},
      {$set:{"name": req.body.name, "note": req.body.note}}, {returnNewDocument: false}, function(err, wrd) {
        if(err) return res.sendStatus(404)
        console.log(err)
        console.log(wrd)
        res.sendStatus(201)
      })
    } else return res.sendStatus(401);
  })
})

router.post('/updateTopic', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyWord(req.body.id)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.name)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.note)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {
      Topic.findOneAndUpdate({_id: req.body.id},
      {$set:{"name": req.body.name, "note": req.body.note}}, {returnNewDocument: false}, function(err, wrd) {
        if(err) return res.sendStatus(404)
        console.log(err)
        console.log(wrd)
        res.sendStatus(201)
      })
    } else return res.sendStatus(401);
  })
})

router.post('/updateWord', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyWord(req.body.id)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.word)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.text)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.explaination)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {
      Word.findOneAndUpdate({_id: req.body.id},
      {$set:{"word": req.body.word, "explaination": req.body.explaination, "text": req.body.text}}, {returnNewDocument: false}, function(err, wrd) {
        if(err) return res.sendStatus(404)
        console.log(err)
        console.log(wrd)
        res.sendStatus(201)
      })
    } else return res.sendStatus(401);
  })
})

router.post('/deleteWord', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyWord(req.body.id)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {
      Word.remove({_id: req.body.id, group: grp._id}, function(err, wrd) {
        if(err) return res.sendStatus(404);
        console.log(err)
        console.log(wrd)
        res.sendStatus(201);
      })
    } else return res.sendStatus(401);
  })
})

// -----------------------------
// FAVORITES API
// -----------------------------

router.get('/fav', ensureAuthenticated, function(req, res) {
  var chosenGroup = req.headers['x-group'];
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      Word.find({group: grp._id, favoriteOf: req.auth.id}, function(err, words) {
        if(err) return res.sendStatus(404);
        res.json(words);
      })
    } else return res.sendStatus(401);
  })
})
router.post('/addToFav', ensureAuthenticated, function(req, res) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyWord(req.body.id)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {
      Word.findOneAndUpdate({_id: req.body.id}, {$addToSet:{favoriteOf: req.auth.id}}, {returnNewDocument: false}, function(err, wrd) {
        if(err) return res.sendStatus(404);
        console.log(err);
        console.log(wrd);
        res.sendStatus(201);
      })
    } else return res.sendStatus(401);
  })
})
router.post('/removeFromFav', ensureAuthenticated, function(req, res) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyWord(req.body.id)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {
      Word.findOneAndUpdate({_id: req.body.id}, {$pull:{favoriteOf: req.auth.id}}, {returnNewDocument: false}, function(err, wrd) {
        if(err) return res.sendStatus(404);
        console.log(err);
        console.log(wrd);
        res.sendStatus(201);
      })
    } else return res.sendStatus(401);
  })
})
// -----------------------------
// GROUPS API
// -----------------------------

// ADD GROUP
router.post('/group', ensureAuthenticated, function(req, res) {
  // sanitize section
  if(!sanitize.verifyString(req.body.name)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.description)) return res.sendStatus(401);
  // eof sanitize section
  let newGroup = new Group({
    name: req.body.name,
    description: req.body.description,
    users: [req.auth.id],
    admins: [req.auth.id],
    createdBy: req.auth.username
  });
  newGroup.save(function(err, group) {
    if(err) return res.sendStatus(503);
    res.json(group);
  })
})

// ADD A USER TO THE GROUP
router.post('/addUserToGroup', ensureAuthenticated, function(req, res) {
  let group = req.body.group, userToAdd = req.body.user, user = req.auth.id;
  // sanitize section
  if(!sanitize.verifyWord(group)) return res.sendStatus(401);
  if(!sanitize.verifyWord(userToAdd)) return res.sendStatus(401);
  // eof sanitize section
  User.findOne({_id: user}, function(err, usr) {
    if(err) return res.sendStatus(401);
    if(!usr) return res.sendStatus(401);
    Group.findOne({_id: group}, function(err, grp) {
      if(err) return res.sendStatus(401);
      if(!grp) return res.sendStatus(401);
      if(grp.admins.indexOf(usr._id)!=-1) {
        Group.updateOne({_id: grp._id}, {$addToSet: {users: [userToAdd]}}, function(err) {
          if(err) return res.sendStatus(401);
          notify(grp.users, 'g', grp.name, req.auth.username, req.auth.id);
          return res.sendStatus(200);
        })
      } else {return res.sendStatus(401);}
    })
  })
})

// REMOVE A USER FROM GROUP
router.post('/removeUserFromGroup', ensureAuthenticated, function(req, res) {
  let group = req.body.group, userToRemove = req.body.user, user = req.auth.id;
  // sanitize section
  if(!sanitize.verifyWord(group)) return res.sendStatus(401);
  if(!sanitize.verifyWord(userToRemove)) return res.sendStatus(401);
  // eof sanitize section
  User.findOne({_id: user}, function(err, usr) {
    if(err) return res.sendStatus(401);
    if(!usr) return res.sendStatus(401);
    Group.findOne({_id: group}, function(err, grp) {
      if(err) return res.sendStatus(401);
      if(!grp) return res.sendStatus(401);
      if(grp.admins.indexOf(usr._id)!=-1||userToRemove==user) {
        if(grp.admins.indexOf(userToRemove)==-1) {
          Group.updateOne({_id: grp._id}, {$pull: {users: userToRemove}}, function(err) {
            if(err) return res.sendStatus(404);
            notify(grp.users, 'gu', grp.name, req.auth.username, req.auth.id);
            return res.sendStatus(200);
          })
        } else return res.sendStatus(401);
      } else return res.sendStatus(401);
    })
  })
})

// ADD A USER TO GROUP'S ADMINS
router.post('/addUserToAdmins', ensureAuthenticated, function(req, res) {
  let group = req.body.group, userToAdd = req.body.user, user = req.auth.id;
  // sanitize section
  if(!sanitize.verifyWord(group)) return res.sendStatus(401);
  if(!sanitize.verifyWord(userToAdd)) return res.sendStatus(401);
  // eof sanitize section
  User.findOne({_id: user}, function(err, usr) {
    if(err) return res.sendStatus(401);
    if(!usr) return res.sendStatus(401);
    Group.findOne({_id: group}, function(err, grp) {
      if(err) return res.sendStatus(401);
      if(!grp) return res.sendStatus(401);
      if(grp.admins.indexOf(usr._id)!=-1) {
        if(grp.users.indexOf(userToAdd)!=-1) {
          Group.updateOne({_id: grp._id}, {$addToSet: {admins: [userToAdd]}}, function(err) {
            if(err) return res.sendStatus(401);
            notify(grp.users, 'a', grp.name, req.auth.username, req.auth.id);
            return res.sendStatus(200);
          })
        } else return res.sendStatus(401);
      } else return res.sendStatus(401);
    })
  })
})

// REMOVE A USER FROM GROUP'S ADMINS
router.post('/removeUserFromAdmins', ensureAuthenticated, function(req, res) {
  let group = req.body.group, userToRemove = req.body.user, user = req.auth.id;
  // sanitize section
  if(!sanitize.verifyWord(group)) return res.sendStatus(401);
  if(!sanitize.verifyWord(userToRemove)) return res.sendStatus(401);
  // eof sanitize section
  User.findOne({_id: user}, function(err, usr) {
    if(err) return res.sendStatus(401);
    if(!usr) return res.sendStatus(401);
    Group.findOne({_id: group}, function(err, grp) {
      if(err) return res.sendStatus(401);
      if(!grp) return res.sendStatus(401);
      if(grp.admins.indexOf(usr._id)!=-1) {
        Group.updateOne({_id: grp._id}, {$pull: {admins: userToRemove}}, function(err) {
          if(err) return res.sendStatus(404);
          notify(grp.users, 'au', grp.name, req.auth.username, req.auth.id);
          return res.sendStatus(200);
        })
      } else {return res.sendStatus(401);}
    })
  })
})

// GET ALL GROUPS THE USER IS IN
router.get('/groups', ensureAuthenticated, function(req, res, next) {
  Group.find({users: req.auth.id}, function(err, groups) {
    if(err) return res.sendStatus(404);
    if(!groups) return res.sendStatus(404);
    res.json(groups);
  })
})

// -----------------------------
// NOTIFICATIONS API
// -----------------------------

// SET A NOTIFICATION
let notify = function(users, type, payload, user, id) {
  id = id.toString();
  for (var i = 0; i < users.length; i++) {
    if(users[i]==id) {
      users.splice(i, 1);
      break;
    }
  }
  if(users.length<=0) return;
  let notification = new Notification({
    users: users,
    type: type,
    payload: payload,
    createdBy: user
  });
  notification.save(function(err, not) {
    if(err) console.log(err);
    console.log(not);
  })
}

// GET NOTIFICATIONS
router.get('/notifications', ensureAuthenticated, function(req, res) {
  Notification.find({users: req.auth.id}, function(err, notifications) {
    if(err) return res.sendStatus(404);
    res.json(notifications);
  })
})

// REMOVE NOTIFICATION
router.get('/notifications/:id', ensureAuthenticated, function(req, res) {
  let id = req.params.id;
  if(!sanitize.verifyWord(id)) return res.sendStatus(401);
  Notification.findOneAndUpdate({_id:id}, {$pull: {users: req.auth.id}}, {returnNewDocument: true}, function(err, newNot) {
    if(err) return res.sendStatus(404);
    else if(newNot.users.length<=1) {
      Notification.deleteOne({_id: newNot._id}, function(err) {
        if(err) return res.sendStatus(404);
        return res.sendStatus(200);
      })
    } else return res.sendStatus(200);
  })
})

// -----------------------------
// HOMEWORK API
// -----------------------------

router.get('/homework', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.headers['x-group'];
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      Homework.find({group: grp._id}, function(err, hw) {
        if(err) return res.sendStatus(404);
        res.json(hw);
      })
    } else return res.sendStatus(401);
  })
})

router.get('/FirstHomework', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.headers['x-group'];
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  // eof sanitize section
  console.log(chosenGroup);
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      Homework.findOne({group: grp._id}, function(err, hw) {
        console.log(hw);
        if(err) return res.sendStatus(404);
        res.json(hw);
      })
    } else return res.sendStatus(401);
  })
})

router.post('/createHomework', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.name)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.description)) return res.sendStatus(401);
  if(!sanitize.verifyString(req.body.deadline)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {          //make sure user is in the group
      let newHomework = new Homework({
        name: req.body.name,
        description: req.body.description,
        deadline: req.body.deadline
      });
      newHomework.group = grp._id;
      newHomework.createdBy = req.auth.username;
      newHomework.save(function(err, word) {
        if(err) return res.send(err)
        notify(grp.users, 'w', grp.name, req.auth.username, req.auth.id);
        res.json(word)
      })
    } else return res.sendStatus(401);
  })
})

router.post('/deleteHomework', ensureAuthenticated, function(req, res, next) {
  var chosenGroup = req.body.chosenGroup;
  // sanitize section
  if(!sanitize.verifyWord(chosenGroup)) return res.sendStatus(401);
  if(!sanitize.verifyWord(req.body.id)) return res.sendStatus(401);
  // eof sanitize section
  Group.findOne({_id: chosenGroup}, function(err, grp) {
    if(err) return res.sendStatus(401);
    if(!grp) return res.sendStatus(401);
    if(grp.users.indexOf(req.auth.id)!=-1) {
      Homework.remove({_id: req.body.id, group: grp._id}, function(err, hw) {
        if(err) return res.sendStatus(404);
        console.log(err)
        console.log(hw)
        res.sendStatus(201);
      })
    } else return res.sendStatus(401);
  })
})


module.exports = router
