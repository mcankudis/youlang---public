app.controller("AbcdCtrl", function ($scope, $http, $state, MainSvc) {
  if(!$scope.username) $state.go('login');
  $scope.$on('ChooseGroupEvent', function(a, grp) {
    $scope.currentGroup = grp;
    getBranches(true);
  })

  $scope.answers = [];
  $scope.points = 0;
  $scope.misses = 0;
  $scope.goNext = false;
  $scope.gameOver = false;
  $scope.chooseAllTopics = function() {
    for(var i = 0; i<$scope.topics.length; i++) {
      $scope.topics[i].inGame = true;
    }
  }
  $scope.unmarkAllTopics = function() {
    for(var i = 0; i<$scope.topics.length; i++) {
      $scope.topics[i].inGame = false;
    }
  }

  $scope.startGame = function() {
    $scope.answers = [];
    $scope.points = 0;
    $scope.misses = 0;
    $scope.goNext = false;
    $scope.gameOver = false;
    $scope.game = true;
    $scope.acceptAnswer = false;
    // console.log(new Date());
    var words = [];
    for(var i = 0; i<$scope.topics.length; i++) {
      if($scope.topics[i].inGame == true) {
        for(var j = 0; j<$scope.words.length; j++) {
          if($scope.words[j].topic==$scope.topics[i]._id) {
            $scope.words[j].isAnswered = false;
            words.push($scope.words[j]);
            // $scope.words.splice(j,1);
          }
        }
      }
    }
    $scope.manyQuestions = words.length - 4;
    $scope.howManyQuestions = words.length - 4;
    // console.log(new Date()); // Is splicing acctually worth it?
    var i = Math.floor(Math.random()*(words.length-1));
    var w = words[i];
    words.splice(i,1);
    var falses = [];
    for(var l = 0; l<words.length; l++) {
      falses.push(words[l]);
    }
    var f = [];
    for(var k = 0; k<3; k++) {
      var r = Math.floor(Math.random()*(falses.length-1));
      f[k] = falses[r].explaination;
      falses.splice(r,1);
    }
    $scope.createQuestion(w.word, w.explaination, f, w, words);
  }

  $scope.createQuestion = function(q, a, f, word, words) {
    if(!q||q==''||!a||a==''||!f||f==[]||!word||word=={}||!words) return MainSvc.errorAlert('Something went wrong. Refresh the page');
    if(words.length<4) {
      MainSvc.successAlert('Game over');
      $scope.result = Math.round(($scope.points/($scope.manyQuestions+$scope.misses)) * 100);
      $scope.game = false;
      $scope.gameOver = true;
      return;
    }
    $scope.acceptAnswer = true;
    var answers = f;
    answers.push(a);
    $scope.question = q;
    for(var k = 0; k<4; k++) {
      var r = Math.floor(Math.random()*(4-k));
      $scope.answers[k]=answers[r];
      answers.splice(r,1);
    }
    $scope.answerQuestion = function(r, id) {
      if(!$scope.acceptAnswer) return;
      if(words.length<4) return MainSvc.successAlert('Game over');
      $scope.acceptAnswer = false;
      if(r==a) {
        $('#'+id).css("background-color", "green");
        $scope.points++;
        $scope.howManyQuestions--;
      } else {
        $('#'+id).css("background-color", "red");
        words.push(word);
        $scope.misses++;
      }
      $scope.goNext=true;
      var i = Math.floor(Math.random()*(words.length-1));
      var w = words[i];
      words.splice(i,1);
      var falses = [];
      for(var l = 0; l<words.length; l++) {
        falses.push(words[l]);
      }
      var f = [];
      for(var k = 0; k<3; k++) {
        var r = Math.floor(Math.random()*(falses.length-1));
        f[k] = falses[r].explaination;
        falses.splice(r,1);
      }
      $scope.nextQ = function() {
        $scope.goNext = false;
        $('#'+id).css("background-color", "white");
        $scope.createQuestion(w.word, w.explaination, f, w, words);
      }
    }
  }

  function getBranches(a) {
    $http.get('/api/branches/', {
      headers: {'x-group': $scope.currentGroup._id}
    }).success(function(branches) {
      $scope.branches = branches;
      if(a) getTopics(true);
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Error');
    })
  }
  function getTopics(a) {
    $http.get('/api/topics/', {
      headers: {'x-group': $scope.currentGroup._id}
    }).success(function(topics) {
      if(a) getWords();
      $scope.topics = topics
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Error', 4000);
    })
  }
  function getWords(callback) {
    $http.get('/api/words/', {
      headers: {'x-group': $scope.currentGroup._id}
    }).success(function(words) {
      $scope.words = words
      if(callback) callback();
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
    })
  }

  if($scope.currentGroup) getBranches(true);
})
