app.service('MainSvc', function($window, $state, $window) {
  var svc = this
  svc.logOut = function() {
    localStorage.clear();
    $window.location.href = '/';
  }
  svc.errorAlert = function(msg) {
    $('.toast-danger').fadeIn().text(msg);
    setTimeout(function() { $('.toast-danger').fadeOut(); }, 4000);
  }
  svc.successAlert = function(msg) {
    $('.toast-success').fadeIn().text(msg);
    setTimeout(function() { $('.toast-success').fadeOut(); }, 4000);
  }
})
