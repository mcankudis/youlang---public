app.controller("DictCtrl", function ($scope, $http, $state, MainSvc) {
  if(!$scope.username) $state.go('login');

  // EVENTS
  $scope.$emit('stateChange', 'dict');
  $scope.$on('ChooseGroupEvent', function(a, grp) {
    $scope.currentGroup = grp;
    getBranches(true);
  })

  // VARIABLES
  $scope.id = localStorage.getItem('id');
  $scope.color1 = localStorage.getItem('color1');
  $scope.color2 = localStorage.getItem('color2');
  $scope.color3 = localStorage.getItem('color3');
  $scope.color4 = localStorage.getItem('color4');
  $scope.chosenTopic = '';
  $scope.reverse = true;
  $scope.sortBy = 'name';

  //FUNCTIONS
  $scope.addSpecialSign = function (a) {
    if(!lastFocused) return;
    lastFocused.value += a;
    lastFocused.focus();
  }
  $scope.addToFav = function(id) {
    $http.post('/api/addToFav/', {
      chosenGroup: $scope.currentGroup._id,
      id: id
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
    });
  }
  $scope.chooseTopic = function(id) {
    $scope.chosenTopic = id;
    if($scope.indexCards) {
      $scope.closeIndexCards();
      getWords($scope.openIndexCards);
    } else {
      getWords();
    }
  }
  $scope.closeIndexCards = function() {
    $scope.indexCards = false;
    $.scrollify.disable();
  }
  $scope.deleteWord = function(id) {
    $http.post('/api/deleteWord/', {
      chosenGroup: $scope.currentGroup._id,
      id: id
    }).success(function(word) {
      getWords();
      MainSvc.successAlert('Word has been removed');
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Error');
    })
    $scope.showDeleteWord = 0;
  }
  $scope.openDeleteWord = function(id) {
    $scope.showDeleteWord = id;
  }
  $scope.openEditWord = function(word) {
    $('#editWordModal').modal('show')
    $scope.editingWord = {}
    $scope.editingWord.word = word.word
    $scope.editingWord.id = word._id
    $scope.editingWord.explaination = word.explaination
    $scope.editingWord.text = word.text
  }
  $scope.openIndexCards = function() {
    $scope.indexCards = true;
    $(document).ready(function() {
      var cards = $('[id*="turnCard"]');
      cards.flip({
        reverse: true
      });
      var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
      if(w<575) {
        $(function() {
          $.scrollify.enable();
          $.scrollify({
            section : ".indexCardScroll",
            interstitialSection: "#indexCardsScrollHeader",
            easing: "easeOutExpo",
            scrollSpeed: 650,
            offset : 0,
            scrollbars: true,
            standardScrollElements: "#collapseOne",
            setHeights: true,
            overflowScroll: true,
            updateHash: false,
            touchScroll: true
          });
        });
      }
    })
  }
  $scope.openInfoWord = function(word) {
    $scope.infoWord = word
    $scope.infoWord.dateS = ""
    for(var i = 0; i<10; i++) {
      $scope.infoWord.dateS += $scope.infoWord.date[i]
    }
    $('#infoWordModal').modal('show')
  }
  $scope.removeFromFav = function(id) {
    $http.post('/api/removeFromFav/', {
      chosenGroup: $scope.currentGroup._id,
      id: id
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
    });
  }
  $scope.searchWords = function() {
    getWords(searchWords);
  }
  function searchWords() {
    if(!$scope.searched) {
      if($scope.indexCards) {
        $scope.closeIndexCards();
        $scope.openIndexCards();
      }
      return;
    }
    if($scope.searched.indexOf('*')!=-1) {
      if($scope.indexCards) {
        $scope.closeIndexCards();
        $scope.openIndexCards();
      }
      return MainSvc.errorAlert('Enter valid search criteria');
    }
    var reg = new RegExp($scope.searched, "i");
    var results = [];
    for(var i = 0; i<$scope.words.length; i++) {
      if(reg.test($scope.words[i].word) || reg.test($scope.words[i].explaination))results.push($scope.words[i]);
    }
    $scope.words = results;
    if($scope.indexCards) {
      $scope.closeIndexCards();
      $scope.openIndexCards();
    }
  }
  $scope.showFav = function() {
    $scope.closeIndexCards();
    $http.get('/api/fav/', {
      headers: {'x-group': $scope.currentGroup._id}
    }).success(function(words) {
      for (var i = 0; i < words.length; i++) {
        words[i].isFav = true;
      }
      $scope.words = words;
      $scope.openIndexCards();
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
    })
  }
  $scope.sort = function(a) {
    if($scope.sortBy==a) $scope.reverse = !$scope.reverse;
    $scope.sortBy = a;
  }
  $scope.toggleFavorites = function(word) {
    if(word.isFav) {
      $scope.removeFromFav(word._id);
    } else {
      $scope.addToFav(word._id);
    }
    word.isFav = !word.isFav;
  }


  $scope.wordsFilter = function(item) {
    if($scope.chosenTopic) {
      if(item.topic==$scope.chosenTopic) {
        return item
      } else {
        return;
      }
    }
    return item;
  };

  function favCheck(words) {
    for (var i = 0; i < words.length; i++) {
      for (var j = 0; j < words[i].favoriteOf.length; j++) {
        if(words[i].favoriteOf[j] == $scope.id) {
          words[i].isFav = true;
        }
      }
    }
  }
  function getBranches(a) {
    $http.get('/api/branches/', {
      headers: {'x-group': $scope.currentGroup._id}
    }).success(function(branches) {
      $scope.branches = branches;
      if(a) getTopics(true);
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Error', 4000);
    })
  }
  function getTopics(a) {
    $http.get('/api/topics/', {
      headers: {'x-group': $scope.currentGroup._id}
    }).success(function(topics) {
      if(a) getWords();
      $scope.topics = topics
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Error', 4000);
    })
  }
  function getWords(callback) {
    $http.get('/api/words/', {
      headers: {'x-group': $scope.currentGroup._id}
    }).success(function(words) {
      favCheck(words);
      $scope.words = words
      if(callback) callback();
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
    })
  }

  if($scope.currentGroup) getBranches(true);

  function setColors() {
    var c1 = localStorage.getItem('color1'),
    c2 = localStorage.getItem('color2'),
    c3 = localStorage.getItem('color3'),
    c4 = localStorage.getItem('color4');
    $scope.C1 = {'background-color': c1, 'color': '#000000'};
    $scope.C2 = {'background-color': c2, 'color': '#000000'};
    $scope.C3 = {'background-color': c3, 'color': '#000000'};
    $scope.C4 = {'background-color': c4, 'color': '#000000'};
    // later the text colors will come from visibility detection AI api
  }
  setColors();
  $(document).ready(function() {
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
  })
});
