app.controller("HomeworkCtrl", function ($scope, $http, $state, MainSvc) {
  if(!$scope.username) $state.go('login');
  $scope.$emit('stateChange', 'homework');
  $scope.$on('ChooseGroupEvent', function(a, grp) {
    $scope.currentGroup = grp;
    getHomework();
  })
  $scope.addHomework = function() {
    console.log($scope.newName);
    $http.post('/api/createHomework/', {
      chosenGroup: $scope.currentGroup._id,
      name: $scope.newName,
      description: $scope.newDescription,
      deadline: $scope.newDeadline
    }).success(function(hw) {
      $scope.homework.push(hw)
      MainSvc.successAlert('Homework has been added')
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Error', 4000);
    })
    $scope.newName = "";
    $scope.newDescription = "";
    $scope.newDeadline = "";
  }
  $scope.deleteHomework = function(id) {
    $http.post('/api/deleteHomework/', {
      chosenGroup: $scope.currentGroup._id,
      id: id
    }).success(function(word) {
      getHomework();
      MainSvc.successAlert('Homework has been removed');
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Error');
    })
  }
  function getHomework() {
    $http.get('/api/homework/', {
      headers: {'x-group': $scope.currentGroup._id}
    }).success(function(homework) {
      $scope.homework = homework;
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Error', 4000);
    })
  }
  if($scope.currentGroup) getHomework();
})
