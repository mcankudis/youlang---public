app.controller('LoginCtrl', function($scope, $http, $window, $state, MainSvc) {
  $scope.$emit('stateChange', 'login');
  $scope.signIn = function() {
    if(!$scope.login || $scope.login==='') {
      MainSvc.errorAlert('Username missing!')
      return;
    }
    if(!$scope.password || $scope.password==='') {
      MainSvc.errorAlert('Password missing!')
      return;
    }
    $http.post('/login/', {
      username: $scope.login,
      password: $scope.password
    }).success(function(obj) {
      console.log(obj);
      localStorage.setItem('token', obj.token);
      localStorage.setItem('username', obj.user.username);
      localStorage.setItem('id', obj.user._id);
      localStorage.setItem('color1', obj.user.settings.color1);
      localStorage.setItem('color2', obj.user.settings.color2);
      localStorage.setItem('color3', obj.user.settings.color3);
      localStorage.setItem('color4', obj.user.settings.color4);
      MainSvc.successAlert('Login succesful');
      $scope.$emit('loginEvent');
      $state.go('user');
    }).error(function(err) {
      MainSvc.errorAlert(err);
    });
  }
  $scope.resetPassword = function() {
    console.log('test');
    if(!$scope.email || $scope.email == '') {
      return MainSvc.errorAlert('Enter Your email adress');
    }
    $http.post('/resetPassword/', {
      email: $scope.email,
      lang: 1
    }).success(function(){
      return MainSvc.successAlert('Further instructions have been sent to the entered email');
    }).error(function(err) {
      if(err=="I'm a teapot") return MainSvc.errorAlert('Unknown email adress');
      return MainSvc.errorAlert('Error, try again later or contact the administrator');
    })
  }
})
