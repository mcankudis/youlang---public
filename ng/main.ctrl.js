app.controller('MainCtrl', function($scope, $http, $state, MainSvc){
  $.scrollify.disable();
  $scope.$on('stateChange', function(meta, data) {
    $scope.currentState = data
  })
  $scope.$on('AddGroupEvent', function(meta, group) {
    $scope.groups.push(group);
  })
  $scope.$on('colorsEvent', function() {
    setColors();
  })
  $scope.$on('loginEvent', function() {
    main();
  })

  $scope.logOut = function() {
    MainSvc.logOut();
  }
  $scope.chooseGroup = function(group) {
    $scope.currentGroup = group;
    $scope.$broadcast('ChooseGroupEvent', $scope.currentGroup);
  }

  function main() {
    var token = localStorage.getItem('token');
    if(!token) $state.go('login');
    else {
      $http.defaults.headers.common['x-auth'] = token;
      $scope.username = localStorage.getItem('username');
      setColors();
      $http.get('/api/groups/').success(function(groups) {
        $scope.groups = groups;
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        $state.go('login')
      })
    }
  }

  function setColors() {
    var c1 = localStorage.getItem('color1'),
    c2 = localStorage.getItem('color2'),
    c3 = localStorage.getItem('color3'),
    c4 = localStorage.getItem('color4');
    $scope.C1 = {'background-color': c1, 'color': '#000000'};
    $scope.C2 = {'background-color': c2, 'color': '#000000'};
    $scope.C3 = {'background-color': c3, 'color': '#000000'};
    $scope.C4 = {'background-color': c4, 'color': '#000000'};
    // later the text colors will come from visibility detection AI api
  }
  main();
})
