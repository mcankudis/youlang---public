app.config(function($stateProvider) {

  var abcdState = {
    name: 'abcd',
    url: '/multipleChoiceTest',
    templateUrl: 'templates/abcd.html',
    controller: 'AbcdCtrl'
  }
  $stateProvider.state(abcdState);

  var dictionaryState = {
    name: 'dictionary',
    url: '/dictionary',
    templateUrl: 'templates/dictionary.html',
    controller: 'DictCtrl'
  }
  $stateProvider.state(dictionaryState);

  var groupsState = {
    name: 'groups',
    url: '/groups',
    templateUrl: 'templates/groups.html',
    controller: 'GrpsCtrl'
  }
  $stateProvider.state(groupsState);

  var homeState = {
    name: 'home',
    url: '/',
    templateUrl: 'templates/home.html',
    controller: 'HomeCtrl'
  }
  $stateProvider.state(homeState);

  var homeworkState = {
    name: 'homework',
    url: '/homework',
    templateUrl: 'templates/homework.html',
    controller: 'HomeworkCtrl'
  }
  $stateProvider.state(homeworkState);

  var loginState = {
    name: 'login',
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl'
  }
  $stateProvider.state(loginState);

  var registerState = {
    name: 'register',
    url: '/register',
    templateUrl: 'templates/register.html',
    controller: 'RegisterCtrl'
  }
  $stateProvider.state(registerState);

  var startState = {
    name: 'user',
    url: '/user',
    templateUrl: 'templates/user.html',
    controller: 'UserCtrl'
  }
  $stateProvider.state(startState);

  var topicsState = {
    name: 'topics',
    url: '/topics',
    templateUrl: 'templates/topics.html',
    controller: 'TopicCtrl'
  }
  $stateProvider.state(topicsState);

});
