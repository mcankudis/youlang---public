app.controller("GrpsCtrl", function ($scope, $http, $state, MainSvc) {
  if(!$scope.username) $state.go('login');
  $scope.$emit('stateChange', 'grps')

  $.scrollify.disable();
  $scope.tmp = {}
  $scope.openedGroup = {};
  $scope.id = localStorage.getItem('id');
  $scope.color1 = localStorage.getItem('color1');
  $scope.color2 = localStorage.getItem('color2');

  $scope.addGroup = function() {
    if(!$scope.newGroup.name || $scope.newGroup.name == '') {
      MainSvc.errorAlert('Name is missing!');
      return;
    }
    $http.post('/api/group/', {
      name: $scope.newGroup.name,
      description: $scope.newGroup.description
    }).success(function(group) {
      $scope.groups.unshift(group)
      $scope.$emit('AddGroupEvent', group);
      $scope.newGroup = {}
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Failed to add the group');
    })
  }

  $scope.addUserToGroup = function(user) {
    var url = '/user/' + user;
    $http.get(url).success(function(obj) {
      for (var i = 0; i < $scope.openedGroup.users.length; i++) {
        if($scope.openedGroup.users[i].username===obj.username) return MainSvc.errorAlert('This user is already a member of this group');
      }
      $http.post('/api/addUserToGroup/', {
        group: $scope.openedGroup._id,
        user: obj._id
      }).success(function() {
        MainSvc.successAlert('Member has been added');
        $scope.openedGroup.users.push(obj)
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        MainSvc.errorAlert('Failed to add the member');
      })
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert("User can't be found");
    })
  }

  $scope.removeUserFromGroup = function(user) {
    $http.post('/api/removeUserFromGroup/', {
      group: $scope.openedGroup._id,
      user: user._id
    }).success(function() {
      MainSvc.successAlert('Member has been removed')
      getGroups();
      $scope.openGroup($scope.openedGroup);
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Failed to remove the member');
    })
  }

  $scope.addUserToAdmins = function(user) {
    $http.post('/api/addUserToAdmins/', {
      group: $scope.openedGroup._id,
      user: user._id
    }).success(function() {
      MainSvc.successAlert("Member has been appointed as admin")
      $scope.openedGroup.admins.push(user)
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Failed to appoint this member as admin');
    })
  }
  $scope.checkGrpIfAdmin = function(group) {
    for (var i = 0, len = group.admins.length; i < len; i++) {
      if(group.admins[i]._id==$scope.id) return true;
    }
    return false;
  }
  $scope.checkIfAdmin = function(id) {
    for (var i = 0, len = $scope.openedGroup.admins.length; i < len; i++) {
      if($scope.openedGroup.admins[i]._id==id) return true;
    }
    return false;
  }

  $scope.removeUserFromAdmins = function(user) {
    $http.post('/api/removeUserFromAdmins/', {
      group: $scope.openedGroup._id,
      user: user._id
    }).success(function() {
      MainSvc.successAlert("Admin removed");
      getGroups();
      $scope.openGroup($scope.openedGroup);
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.successAlert('Failed to remove the admin');
    })
  }

  $scope.openGroup = function(group) {
    if(!$scope.openedGroup) {
      var tmp = {};
      tmp.users = group.users;
      tmp.admins = group.admins;
      group.users = [];
      group.admins = [];
      $scope.openedGroup = group;
      resolveUsers(tmp);
    }else if($scope.openedGroup._id===group._id) {
      $scope.openedGroup = null;
    }else {
      var tmp = {};
      tmp.users = group.users;
      tmp.admins = group.admins;
      group.users = [];
      group.admins = [];
      $scope.openedGroup = group;
      resolveUsers(tmp);
    }
  }
  var getGroups = function() {                                    // It's a crazy function because every
    $http.get('/api/groups/').success(function(groups) {           // user and admin has to be converted
      $scope.groups = groups;                                     // from just an id in an array to
      for (var i = 0; i < $scope.groups.length; i++) {            // an object in form of: {_id: "someID"}
        var tmp = [];                                             // exactly here, before any operations
        for (var j = 0; j < $scope.groups[i].users.length; j++) { // on users and admins occure
          tmp.push({_id: $scope.groups[i].users[j]});
        }
        $scope.groups[i].users = tmp;
        tmp = [];
        for (var j = 0; j < $scope.groups[i].admins.length; j++) {
          tmp.push({_id: $scope.groups[i].admins[j]});
        }
        $scope.groups[i].admins = tmp;
      }
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      $scope.groups = {};
    })
  }

  var resolveUsers = function(group) {
    for (var i = 0; i < group.users.length; i++) {
      var url = '/users/' + group.users[i]._id;
      $http.get(url).success(function(obj) {
        $scope.openedGroup.users.push(obj);
      }).error(function() {
        if(err==="Unauthorized") MainSvc.logOut();
        $scope.openedGroup.users.push(group.users[i] = {username: 'unknown', _id: group.users[i]._id});
      })
    }
    for (var i = 0; i < group.admins.length; i++) {
      var url = '/users/' + group.admins[i]._id;
      $http.get(url).success(function(obj) {
        $scope.openedGroup.admins.push(obj);
      }).error(function() {
        if(err==="Unauthorized") MainSvc.logOut();
        $scope.openedGroup.admins.push(group.users[i] = {username: 'unknown', _id: group.users[i]._id});
      })
    }
  }

  getGroups();

})
