app.controller("UserCtrl", function ($scope, $http, $state, MainSvc) {
  if(!$scope.username) $state.go('login');
  $scope.$emit('stateChange', 'user')
  $scope.$on('ChooseGroupEvent', function(a, grp) {
    $scope.currentGroup = grp;
    getHomework();
  })
  $.scrollify.disable();
  $scope.wordsNotifications = {};
  $scope.counter = 0;
  $scope.colorsD = false;
  $scope.passD = false;
  $scope.username = localStorage.getItem('username');
  $scope.color1 = localStorage.getItem('color1');
  $scope.color2 = localStorage.getItem('color2');
  $scope.color3 = localStorage.getItem('color3');
  $scope.color4 = localStorage.getItem('color4');
  function getNotifications() {
    $http.get('/api/notifications/').success(function(notifications) {
      $scope.notifications = notifications;
      for (var i = 0; i < $scope.notifications.length; i++) {
        $scope.counter++;
        if($scope.notifications[i].type=='w') {
          if(!$scope.wordsNotifications[$scope.notifications[i].payload]) {
            $scope.wordsNotifications[$scope.notifications[i].payload] = [];
            $scope.wordsNotifications[$scope.notifications[i].payload].counter = 0;
          }
          $scope.wordsNotifications[$scope.notifications[i].payload].push($scope.notifications[i]);
          $scope.wordsNotifications[$scope.notifications[i].payload].counter++;
        }
      }
    })
  }
  function getHomework() {
    $http.get('/api/FirstHomework/', {
      headers: {'x-group': $scope.currentGroup._id}
    }).success(function(homework) {
      $scope.homework = homework;
    }).error(function(err) {
      if(err==="Unauthorized") MainSvc.logOut();
      MainSvc.errorAlert('Error', 4000);
    })
  }
  getNotifications();
  if($scope.currentGroup) getHomework();
  $scope.logOut = function() {
    MainSvc.logOut();
  }
  $scope.colors = function() {
    if(!$scope.colorsD) $('#colors').fadeIn();
    else $('#colors').fadeOut();
    $scope.colorsD = !$scope.colorsD;
  }
  $scope.pass = function() {
    if(!$scope.passD) $('#pass').fadeIn();
    else $('#pass').fadeOut();
    $scope.passD = !$scope.passD;
  }
  $scope.changePassword = function() {
    if(!$scope.password || $scope.password == '') return MainSvc.errorAlert('Enter old password');
    if(!$scope.password || $scope.passwordNew == '') return MainSvc.errorAlert('Enter new password');
    if(!$scope.passwordNew2 || $scope.passwordNew2 == '') return MainSvc.errorAlert('Repeat the new password');
    if($scope.passwordNew!=$scope.passwordNew2) return MainSvc.errorAlert('Passwords do not match');
    $http.post('/changePassword/', {
      password: $scope.password,
      newPassword: $scope.passwordNew
    }).success(function() {
      MainSvc.successAlert('Password has been changed');
      $('#pass').fadeOut();
    }).error(function(err) {
      MainSvc.errorAlert('Error');
      if(err==="Unauthorized") MainSvc.logOut();
    })
  }
  $scope.dismissNotification = function(id) {
    $http.get('/api/notifications/'+id).success(function() {
      $scope.counter--;
    });
  }
  $scope.dismissWordsNotifications = function(grp) {
    for (var i = 0; i < $scope.wordsNotifications[grp].length; i++) {
      $http.get('/api/notifications/'+$scope.wordsNotifications[grp][i]._id);
      $scope.counter--;
    }
  }
  $scope.goToHw = function() {
    $state.go('homework')
  }
  $scope.updateColorSettings = function() {
    if(!$scope.color1 || $scope.color1 == '') {
      return;
    }
    if(!$scope.color2 || $scope.color2 == '') {
      return;
    }
    if(!$scope.color3 || $scope.color3 == '') {
      return;
    }
    if(!$scope.color4 || $scope.color4 == '') {
      return;
    }
    $http.post('/updColorSettings/', {
      color1: $scope.color1,
      color2: $scope.color2,
      color3: $scope.color3,
      color4: $scope.color4
    }).success(function() {
      MainSvc.successAlert('Color settings have been updated');
      $('#colors').fadeOut();
      localStorage.setItem('color1', $scope.color1);
      localStorage.setItem('color2', $scope.color2);
      localStorage.setItem('color3', $scope.color3);
      localStorage.setItem('color4', $scope.color4);
      $scope.$emit('colorsEvent');
    }).error(function(err) {
      MainSvc.errorAlert('Wystąpił błąd');
      if(err==="Unauthorized") MainSvc.logOut();
    })
  }

});
