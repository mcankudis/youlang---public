app.controller('RegisterCtrl', function($scope, $http, $state, MainSvc){
  $scope.$emit('stateChange', 'register');
  $scope.register = function() {
    if(!$scope.login || $scope.login==='') {
      MainSvc.errorAlert('Username missing!')
      return;
    }
    if(!$scope.password || $scope.password==='') {
      MainSvc.errorAlert('Password missing!')
      return;
    }
    if(!$scope.password2 || $scope.password2==='') {
      MainSvc.errorAlert('Repeat the password')
      return;
    }
    if($scope.password !== $scope.password2) {
      MainSvc.errorAlert('Passwords do not match!')
      return;
    }
    if(!$scope.email || $scope.email==='') {
      MainSvc.errorAlert('Email adress is missing!')
      return;
    }
    $http.post('/register/', {
      email: $scope.email,
      username: $scope.login,
      password: $scope.password,
      password2: $scope.password2
    }).success(function(res) {
      if(res[0].msg) {
        MainSvc.errorAlert(res);
      } else {
        MainSvc.successAlert('Account successfully created, You can now log in')
        $state.go('login');
      }
    }).error(function(err) {
      if(err==="I'm a teapot") MainSvc.errorAlert('Username or Email already taken');
      else MainSvc.errorAlert('Error, try again');
    });
  }

})
