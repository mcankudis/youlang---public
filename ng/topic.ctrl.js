app.controller("TopicCtrl", function ($scope, $location, $http, $state, MainSvc) {
  if(!$scope.username) $state.go('login');
    // EVENTS
    $scope.$emit('stateChange', 'topic')

    $scope.$on('ChooseGroupEvent', function(a, grp) {
      $scope.currentGroup = grp;
      getBranches(true);
    })

    // SCOPE VARIABLES
    $scope.id = localStorage.getItem('id');
    $scope.color1 = localStorage.getItem('color1');
    $scope.color2 = localStorage.getItem('color2');
    $scope.color3 = localStorage.getItem('color3');
    $scope.newWord = {};
    $scope.newTopic = {};
    $scope.newBranch = {};
    $scope.hgt = 100;
    $scope.reverse = true;
    $scope.sortBy = 'name';
    $scope.showDeleteWord = 0;
    var lastFocused;
    $.scrollify.disable();

    // MAIN
    if(!$scope.username) $state.go('login');
    if($scope.currentGroup) getBranches(true);

    // FUNCTIONS
    $scope.addBranch = function() {
      if(!$scope.newBranch.name || $scope.newBranch.name == '') {
        MainSvc.errorAlert('Name missing!');
        return;
      }
      if(!$scope.newBranch.bIndexNr || $scope.newBranch.bIndexNr == '') {
        MainSvc.errorAlert('Index number missing');
        return;
      }
      var i = 0;
      $scope.newBranch.htmlId = 'item-' + $scope.newBranch.bIndexNr
      while(i<$scope.branches.length) {
        if($scope.branches[i].htmlId==$scope.newBranch.htmlId) {
          MainSvc.errorAlert('Branch with this index number already exists!');
          return;
        }
        i++;
      }
      $http.post('/api/createBranch/', {
        chosenGroup: $scope.currentGroup._id,
        name: $scope.newBranch.name,
        htmlId: $scope.newBranch.htmlId,
        bIndexNr: $scope.newBranch.bIndexNr,
        note: $scope.newBranch.note
      }).success(function(branch) {
        $scope.branches.push(branch)
        $scope.hgt += 41;
        MainSvc.successAlert('Branch has been created')
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        MainSvc.errorAlert('Error', 4000);
      })
      $scope.newBranch = {}
    }
    $scope.addToFav = function(id) {
      $http.post('/api/addToFav/', {
        chosenGroup: $scope.currentGroup._id,
        id: id
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
      });
    }
    $scope.addSpecialSign = function (a) {
      if(!lastFocused) return;
      lastFocused.value += a;
      lastFocused.focus();
    }
    $scope.addTopic = function() {
      $scope.newTopic.htmlId = '';
      $scope.newTopic.htmlId = $scope.newTopic.htmlIdInt.toString();
      // tests
      if(!$scope.newTopic.name || $scope.newTopic.name == '') {
        MainSvc.errorAlert('Name missing!');
        return;
      }
      if(!$scope.newTopic.htmlId || $scope.newTopic.htmlId == '') {
        MainSvc.errorAlert('Index number missing!');
        return;
      }
      if(!$scope.newTopic.parentBranch || $scope.newTopic.parentBranch == '') {
        MainSvc.errorAlert('No branch has been chosen!');
        return;
      }
      var i = 0; // creating the id attribute:
      var tmpNr;
      while(i<$scope.branches.length) {
        if($scope.branches[i]._id == $scope.newTopic.parentBranch) {
          tmpNr = $scope.branches[i].htmlId + '-';
        }
        i++;
      }
      $scope.newTopic.htmlId = tmpNr+$scope.newTopic.htmlId;
      i = 0; // checking if there is already a topic with this order number:
      while(i<$scope.topics.length) {
        if($scope.topics[i].parentBranch==$scope.newTopic.parentBranch && $scope.topics[i].htmlId==$scope.newTopic.htmlId) {
          return MainSvc.errorAlert('Topic with this index number already exists!');
        }
        i++;
      }
      $scope.newTopic.indexNr = $scope.newTopic.htmlId[5] + '.' + $scope.newTopic.htmlId[7]
      $http.post('/api/createTopic/', {
        chosenGroup: $scope.currentGroup._id,
        name: $scope.newTopic.name,
        htmlId: $scope.newTopic.htmlId,
        indexNr: $scope.newTopic.indexNr,
        parentBranch: $scope.newTopic.parentBranch,
        note: $scope.newTopic.note
      }).success(function(topic) {
        $scope.topics.push(topic)
        $scope.hgt += 41;
        MainSvc.successAlert('Topic has been created')
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        MainSvc.errorAlert('Error', 4000);
      })
      $scope.newTopic = {};
    }
    $scope.addWord = function() {
      if(!$scope.chosenTopic) {
        MainSvc.errorAlert('No topic has been chosen');
        return;
      }
      if(!$scope.newWord.word || $scope.newWord.word == '') {
        MainSvc.errorAlert('Word missing!');
        return;
      }
      if(!$scope.newWord.explaination || $scope.newWord.explaination == '') {
        MainSvc.errorAlert('Explaination missing!');
        return;
      }
      $scope.newWord.topic = $scope.chosenTopic._id
      $http.post('/api/createWord/', {
        chosenGroup: $scope.currentGroup._id,
        word: $scope.newWord.word,
        explaination: $scope.newWord.explaination,
        topic: $scope.newWord.topic,
        text: $scope.newWord.text
      }).success(function(word) {
        $scope.words.push(word)
        MainSvc.successAlert('Word has been created')
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        MainSvc.errorAlert('Error', 4000);
      })
      $scope.newWord = {}
    }
    $scope.backToTopicsMenu = function() {
      $scope.chosenTopic = undefined
    }
    $scope.chooseTopic = function(topic) {
      $scope.chosenTopic = topic;
    }
    $scope.deleteWord = function(id) {
      $http.post('/api/deleteWord/', {
        chosenGroup: $scope.currentGroup._id,
        id: id
      }).success(function(word) {
        getWords();
        MainSvc.successAlert('Word has been deleted')
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        MainSvc.errorAlert('Error', 4000);
      })
      $scope.showDeleteWord = 0;
    }
    $scope.goToTopic = function(id) {
      id = '#'+id
      $location.url(id)
    }
    $scope.openAddWord = function() {
      $('#addWordModal').modal('show')
    }
    $scope.openDeleteWord = function(id) {
      $scope.showDeleteWord = id;
    }
    $scope.openEditBranch = function(branch) {
      $('#editBranchModal').modal('show')
      $scope.editingBranch = {}
      $scope.editingBranch.name = branch.name
      $scope.editingBranch.id = branch._id
      $scope.editingBranch.note = branch.note
    }
    $scope.openEditTopic = function(topic) {
      $('#editTopicModal').modal('show')
      $scope.editingTopic = {}
      $scope.editingTopic.name = topic.name
      $scope.editingTopic.id = topic._id
      $scope.editingTopic.note = topic.note
    }
    $scope.openEditWord = function(word) {
      $('#editWordModal').modal('show')
      $scope.editingWord = {}
      $scope.editingWord.word = word.word
      $scope.editingWord.id = word._id
      $scope.editingWord.explaination = word.explaination
      $scope.editingWord.text = word.text
    }
    $scope.openInfoWord = function(word) {
      $scope.infoWord = word
      $scope.infoWord.dateS = ""
      for(var i = 0; i<10; i++) {
        $scope.infoWord.dateS += $scope.infoWord.date[i]
      }
      $('#infoWordModal').modal('show')
    }
    $scope.removeFromFav = function(id) {
      $http.post('/api/removeFromFav/', {
        chosenGroup: $scope.currentGroup._id,
        id: id
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
      });
    }
    $scope.sort = function(a) {
      if($scope.sortBy==a) $scope.reverse = !$scope.reverse;
      $scope.sortBy = a;
    }
    $scope.saveEditBranch = function() {
      if(!$scope.editingBranch.name||$scope.editingBranch.name=='') {
        MainSvc.errorAlert('Name missing!');
        return;
      }
      $http.post('/api/updateBranch/', {
        chosenGroup: $scope.currentGroup._id,
        id: $scope.editingBranch.id,
        name: $scope.editingBranch.name,
        note: $scope.editingBranch.note
      }).success(function(branch) {
        getBranches();
        MainSvc.successAlert('Branch has been updated');
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        $state.reload();
        MainSvc.errorAlert('Error. Try again');
      })
      $scope.editingBranch = {};
    }
    $scope.saveEditTopic = function() {
      if(!$scope.editingTopic.name||$scope.editingTopic.name=='') {
        MainSvc.errorAlert('Name missing!');
        return;
      }
      $http.post('/api/updateTopic/', {
        chosenGroup: $scope.currentGroup._id,
        id: $scope.editingTopic.id,
        name: $scope.editingTopic.name,
        note: $scope.editingTopic.note
      }).success(function(topic) {
        getTopics();
        MainSvc.successAlert('Topic has been updated');
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        $state.reload();
        MainSvc.errorAlert('Error. Try again');
      })
      $scope.editingBranch = {};
    }
    $scope.saveEditWord = function() {
      if(!$scope.editingWord.word||$scope.editingWord.word=='') {
        MainSvc.errorAlert('Word missing!');
        return;
      }
      if(!$scope.editingWord.explaination||$scope.editingWord.explaination=='') {
        MainSvc.errorAlert('Explaination missing!');
        return;
      }
      $http.post('/api/updateWord/', {
        chosenGroup: $scope.currentGroup._id,
        id: $scope.editingWord.id,
        word: $scope.editingWord.word,
        explaination: $scope.editingWord.explaination,
        text: $scope.editingWord.text
      }).success(function(word) {
        getWords();
        MainSvc.successAlert('Word has been updated')
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        $state.reload();
        MainSvc.errorAlert('Error. Try again');
      })
      $scope.editingWord = {};
    }
    $scope.toggleFavorites = function(word) {
      if(word.isFav) {
        $scope.removeFromFav(word._id);
      } else {
        $scope.addToFav(word._id);
      }
      word.isFav = !word.isFav;
    }

    function favCheck(words) {
      for (var i = 0; i < words.length; i++) {
        for (var j = 0; j < words[i].favoriteOf.length; j++) {
          if(words[i].favoriteOf[j] == $scope.id) {
            words[i].isFav = true;
          }
        }
      }
    }
    function getBranches(a) {
      $http.get('/api/branches/', {
        headers: {'x-group': $scope.currentGroup._id}
      }).success(function(branches) {
        $scope.branches = branches;
        if(a) getTopics(true);
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        MainSvc.errorAlert('Error', 4000);
      })
    }
    function getTopics(a) {
      $http.get('/api/topics/', {
        headers: {'x-group': $scope.currentGroup._id}
      }).success(function(topics) {
        if(a) getWords();
        $scope.topics = topics
        var branchesHgt = $scope.branches.length || 1
        $scope.hgt = $scope.topics.length*41+branchesHgt*41+100;
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        MainSvc.errorAlert('Error', 4000);
      })
    }
    function getWords() {
      $http.get('/api/words/', {
        headers: {'x-group': $scope.currentGroup._id}
      }).success(function(words) {
        favCheck(words);
        $scope.words = words
      }).error(function(err) {
        if(err==="Unauthorized") MainSvc.logOut();
        MainSvc.errorAlert('Error', 4000);
      })
    }

    // BOOTSTRAP JS & JQUERY
    $(document).ready(function(){
      var inputs = $('[type="text"]');
      for (var i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener("click", function(el){
          lastFocused = el.target;
        });
      }
      $('.topicsScrollspySection').scrollspy({ target: '#topicsNavbar' });
      $('[data-toggle="tooltip"]').tooltip();
    })
});
