## YouLang

##### [Open in browser](youlang.pl)

A simple webapp for language course participants, but also for single learners that allows the management of learning material, including topics and words, also providing learning methods such as index cards and tests.

### Functionality:

Branches/topics/words management <br/>
In-app notifications about changes in groups and new vocabulary <br/>
Groups management (easy sharing) <br/>
Homework management <br/>

---

The project now features Docker. Assuming You have Docker installed, You can simply simply run the app on Your machine by doing the following:
1. Navigate to repo's root directory
2. Create a file named db.js and paste in the following: <br/>
"use strict";
const mongoose = require('mongoose');
mongoose.connect('mongodb://mongo:27017/YouLang2', {useNewUrlParser: true},
function() {
console.log('Nawiązano połączenie z bazą');
})
module.exports = mongoose

3. Create a file named .env and paste in following content, replacing comments with necessary data: <br/>
SECRET=*just a random string* <br/>
SECRET2=*just a random string* <br/>
4. Run `docker-compose up` (Add option `-d` to run in detached mode (background)).
The app should be listening on port 3002.

---

In case You want to run the app on Your device without docker, You need to have nodeJS v8+ and mongodb installed.
Installation steps:

1. Clone the repo to a directory of choice
2. Create a .env file in repo's root folder and paste in following content, replacing comments with necessary data: <br/>
SECRET=*just a random string* <br/>
SECRET2=*just a random string* <br/>
3. Navigate there in command line ('/Your/Directory/youlang')
4. Start the mangodb service
If You're running a debian-based distro type in: `sudo service mongod start`
If You're running an arch-based distro type in: `sudo systemctl start mongodb`
5. Run following commands: <br/>
`npm install` <br/>
`npm start` <br/>
5. The app should be running on localhost:3002

In order to change client-side javascript code, You need to work on files in ng/ directory and then compile them running <br/>
`gulp js`<br/>
`gulp watchjs` compiles starts a terminal process that watches over files in ng/ and compiles the code after a change has been made.

Alternatively, You can just mess up directly with client/scripts/app.js

Warning: when using `npm install` node will automatically install the newest versions of the packages and also update the package.json file. In case You experience any compabilty issues look up the initial version in package.json on github or just redownload the repo, remove the '^' in incompatibile package version and run `npm update` / `npm install`

Also the password recovery service is not going to work on a local machine.
