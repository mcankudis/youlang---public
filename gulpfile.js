'use strict';
const gulp = require('gulp')
const concat = require('gulp-concat')
const ngAnnotate = require('gulp-ng-annotate')
const sourcemaps = require('gulp-sourcemaps')
const uglify = require('gulp-uglify')

let paths = {
  js: {
    head: 'ng/module.js',
    src: 'ng/**/*.js',
    dest: 'client/scripts/'
  }
};

function js() {
  return gulp.src([paths.js.head, paths.js.src])
  .pipe(sourcemaps.init())
  .pipe(ngAnnotate())
  .pipe(uglify())
  .pipe(sourcemaps.write())
  .pipe(concat('app.js'))
  .pipe(gulp.dest(paths.js.dest))
}

gulp.task('js', function() {
    return js();
})

gulp.task('watchjs', function() {
    return gulp.watch('ng/**/*.js', gulp.series(js));
})
