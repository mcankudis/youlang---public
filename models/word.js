"use strict";
const db = require('../db');
const Schema = db.Schema;

const wordSchema = new Schema({
    word:         {type: String, required: true},
    explaination: {type: String, required: true},
    group:        {type: String, required: true},
    topic:        {type: String, required: true},
    text:         {type: String, required: false},
    favoriteOf:   {type: Array, required: true, default: []},
    createdBy:    {type: String, required: true},
    date:         {type: Date, default: Date.now, required: true}
});

const Word = db.model('word', wordSchema);
module.exports = Word;
