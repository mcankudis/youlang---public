"use strict";
const db = require('../db');
const Schema = db.Schema;

const topicSchema = new Schema({
    name:         {type: String, required: true},
    htmlId:       {type: String, required: true},
    indexNr:      {type: String, required: true},
    parentBranch: {type: String, required: true},
    group:        {type: String, required: true},
    note:         {type: String},
    createdBy:    {type: String, required: true},
    date:         {type: Date, default: Date.now, required: true}
});

const Topic = db.model('topic', topicSchema);
module.exports = Topic;
