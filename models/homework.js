"use strict";
const db = require('../db');
const Schema = db.Schema;

const homeworkSchema = new Schema({
    name:         {type: String, required: false},
    description:  {type: String, required: false},
    group:        {type: String, required: true},
    deadline:     {type: Date, required: false},
    createdBy:    {type: String, required: true},
    date:         {type: Date, default: Date.now, required: true}
});

const Homework = db.model('homework', homeworkSchema);
module.exports = Homework;
