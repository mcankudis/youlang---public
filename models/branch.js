"use strict";
const db = require('../db');
const Schema = db.Schema;

const branchSchema = new Schema({
    name:         {type: String, required: true},
    htmlId:       {type: String, required: true},
    group:        {type: String, required: true},
    bIndexNr:     {type: Number, required: true},
    note:         {type: String},
    createdBy:    {type: String, required: true},
    date:         {type: Date, default: Date.now, required: true}
});

const Branch = db.model('branch', branchSchema);
module.exports = Branch;
