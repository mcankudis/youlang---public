"use strict";
const db = require('../db');
const Schema = db.Schema;

const notificationSchema = new Schema({
    users:        {type: Array, required: true},
    type:         {type: Array, required: true},
    payload:      {type: String, required: true},
    createdBy:    {type: String, required: true},
    date:         {type: Date, default: Date.now, required: true}
});

const Notification = db.model('notification', notificationSchema);
module.exports = Notification;
