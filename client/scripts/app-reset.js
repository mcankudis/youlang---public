var app = angular.module('YouLang', []);
app.controller('MainCtrl', function($scope, $http){
  $scope.save = function() {
    var params = (new URL(document.location)).searchParams;
    var token = params.get("seq");
    console.log(token);
    if(!$scope.password || $scope.password == '') return errorAlert('Podaj nowe hasło');
    if(!$scope.password2 || $scope.password2 == '') return errorAlert('Powtórz hasło');
    if($scope.password!=$scope.password2) return errorAlert('Hasła nie są identyczne');
    $http.post('/resetChangePass', {
      token: token,
      password: $scope.password
    }).success(function() {
      successAlert('Hasło zostało zmienione. Możesz się zalogować');
      window.location.href = 'https://youlang.pl/#/login';
    }).error(function() {
      errorAlert('Wystąpił błąd. Spróbuj ponownie.')
    })
  }
  function errorAlert(msg) {
    $('.toast-danger').fadeIn().text(msg);
    setTimeout(function() { $('.toast-danger').fadeOut(); }, 4000);
  }
  function successAlert(msg) {
    $('.toast-success').fadeIn().text(msg);
    setTimeout(function() { $('.toast-success').fadeOut(); }, 4000);
  }
})
