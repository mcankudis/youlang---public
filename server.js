'use strict';
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const expressValidator = require('express-validator');
const passport = require('passport');
const helmet = require('helmet');
const keys = require('./config/keys');
const passportSetup = require('./config/passport-setup');

// Load enviroment variables
require('dotenv').config();

// Security headers
app.use(helmet());

// Body parse
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// Express Validator
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      let namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

// initialize passport
app.use(passport.initialize());

//Set static folder
app.use(express.static(__dirname + '/client'));

// Set the view engine
app.set('views', __dirname + '/client');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// These are the routes
const index = require('./routes/index');
const api = require('./routes/api');
app.use('/', index);
app.use('/api', api);

const port = 3002;
app.listen(port, function() {
  console.log('['+new Date+'] Hi, listening on port ' + port);
})
